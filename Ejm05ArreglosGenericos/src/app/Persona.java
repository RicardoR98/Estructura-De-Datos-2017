package app;

public class Persona {
	private String nombre;
	private int edad;
	private boolean genero;

	public Persona() {
		this.nombre = "John Doe";
		this.edad = 20;
		this.genero = false;
	}// fin costructor default

	public Persona(String nombre, int edad, boolean genero) {
		this.nombre = nombre;
		this.edad = edad;
		this.genero = genero;
	}// fin constructor con parametros

	@Override
	public String toString() {
		return "Nombre: " + this.nombre + " Edad: " + edad + " Genero: " + genero;
	}

}
