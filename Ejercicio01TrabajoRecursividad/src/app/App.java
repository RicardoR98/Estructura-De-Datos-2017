package app;

public class App {
	
	public static void main(String[] args) {
		
		//int res = (int) Pruebas.Prueba6(101, 0);
		//int res = suma_digi(111, 0);
		//System.out.println(res);
		
	}
	
	static int suma_digi(int n, int sum){ // FUNCIONANDO
		sum += n % 10;
		if(n<=0){
			return sum;
		}else{
			if(n!=0){
				n = n / 10;
				return (suma_digi(n, sum));
			}
		}
		
		return 0;
		
	}
	
	static int numeros(int n, int aux){ // FUNCIONANDO
		
		if(aux==n+1){
			return 0;
		}else{
			System.out.println(aux);
			return numeros(n, ++aux);
		}
		
	}
	
	static int num_digi(int n, int sifras){ //FUNCIONANDO
		if(n<=0){
			return sifras;
		}else{
			if(n!=0){
				n = n / 10;
				return (num_digi(n, ++sifras));
			}
		}
		
		return 0;
	}
	
	static int producto(int a, int b, int c) { //FUNCIONANDO
		if(b <= 0) {
			return c;
		}else {
			c = c + a;
			return producto(a, --b, c);
		}
		
	}
	
	static int potencia(int a, int b) { //FUNCIONANDO
		if (b==0) { 
			return 1;
        } else  { 
        	return a * potencia(a, b-1);
        }
		
	}
	
}