package app;

public class Persona {
	
	private String nombre;
	private int edad;
	private boolean genero;
	
	
	public Persona(){
		this.nombre = "John Doe";
		this.edad = 20;
		this.genero = false;
	}
	
	public Persona(String nombre, int edad, boolean genero){
		this.nombre = nombre;
		this.edad = edad;
		this.genero = genero;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Nombre: " + this.nombre + " Edad: " + this.edad + " Genero: " + this.genero;
	}
	
}