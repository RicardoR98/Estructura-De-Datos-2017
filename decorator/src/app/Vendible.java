package app;

public interface Vendible {
	public String getDescripcion();

	public int getPrecio();
}
