package app;

public class Gasolina extends AutoDecorator {

	public Gasolina(Vendible vendible) {
		super(vendible);
	}

	public String getDescripcion() {
		return getVendible().getDescripcion() + " + Gasolina";
	}

	public int getPrecio() {
		return getVendible().getPrecio() + 1200;
	}
}
