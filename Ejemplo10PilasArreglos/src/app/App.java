package app;

import stack.Stack;

public class App {

	public static void main(String[] args) {
		
		Stack <String> names = new Stack<String>(String.class, 5);
		
		try{
			
		names.push("Kike");
		names.push("Gustavo");
		names.push("Anna");
		names.push("Andrik");
		names.push("Ricardo 2");
		System.out.println(names.pop());
		names.push("Mr. Been");
		System.out.println(names.search("Mr. Been"));
		
		System.out.println("");
		
		for (String a : names) {
			System.out.println(a);
		}
		
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

}