package App;

import java.util.Scanner;

public class App {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num;
        do {
            System.out.print("Introduzca un numero mayor a 0: ");
            num = sc.nextInt();
        } while (num < 0);
        
        System.out.println();
        System.out.print("Binario: "); decBin(num);
    }

    public static void decBin(int n) {
        if (n < 2) {
            System.out.print(n);
            return;
        } else {
            decBin(n / 2);
            System.out.print(n % 2);
            return;
        }
    }

}