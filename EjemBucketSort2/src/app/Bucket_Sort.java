package app;

import java.util.Random;

public class Bucket_Sort {

	static int[] sort(int[] secuencia, int maxValue){
		
		//Bucket Sort
		int[] Bucket = new int [maxValue + 1];
		int[] sort_secuencia = new int[secuencia.length];
		
		for (int i = 0; i < secuencia.length; i++) {
			Bucket[secuencia[i]]++;
		}
		int outPos = 0;
		for (int i = 0; i < Bucket.length; i++) {
			for (int j = 0; j < Bucket[i]; j++) {
				sort_secuencia[outPos++]=i;
			}
		}
		return sort_secuencia;
	}
	
	static void printSecuencia(int[] sort_secuencia){
		for (int i = 0; i < sort_secuencia.length; i++) {
			System.out.println(sort_secuencia[i] + " ");
		}
	}
	
	static int maxValue(int[] secuencia){
		int maxValue = 0;
		for (int i = 0; i < secuencia.length; i++) {
			if(secuencia[i] > maxValue){
				maxValue = secuencia[i];
			}
		}
		return maxValue;
	}
	
	
	public static void main(String[] args) {
		System.out.println("Clasificación de numeros al azar utilizando el Bucket Sort");
		Random random= new Random();
		int N = 10;
		int[]secuencia = new int[N];
		
		for (int i = 0; i < N; i++) {
			secuencia[i] = Math.abs(random.nextInt(100));
			
			int maxValue = maxValue(secuencia);
			
			System.out.println("\n Secuencia Original: ");
			printSecuencia(secuencia);
			System.out.println("\n Secuencia Sort: ");
			printSecuencia(sort(secuencia, maxValue));
			
		}

	}

}
