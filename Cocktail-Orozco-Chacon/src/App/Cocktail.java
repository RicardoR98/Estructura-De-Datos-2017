package App;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
public class Cocktail<T extends Comparable<T>> {
	private int capacity=1;
	private T[] array=null;
	private Class<T> type=null;
	private int contador=0;
	public static int arreglocopia;
	@SuppressWarnings("unchecked")

	//constructor
	public Cocktail(Class<T> clase) {
		this.type = clase;
		array = (T[])Array.newInstance(clase, capacity);
	}
	//llenar el arreglo
	public void add(T value){
		try {
			array[contador] = value;
			contador++;
		} catch (ArrayIndexOutOfBoundsException e) {
		
	 		array= Arrays.copyOf(array, array.length+1);
			array[contador]=value;
			contador++;
			capacity++;
		}
	}
	//metodo de ordenamiento
	public T[] CocktailSort(){ 
		T[] arreglocopia= Arrays.copyOf(array, array.length);
		boolean bandera =true;
		int i=0;
		int j=arreglocopia.length-1;
		while(i<j &&bandera) {
			
			for(int k=0;k<j;k++){ 
			
				if(arreglocopia[k+1].compareTo(arreglocopia[k])<=0 ){ 
				T aux = arreglocopia[k]; 
				arreglocopia[k] = arreglocopia[k+1]; 
				arreglocopia[k+1] = aux; 
				bandera=true;
				} 
				} 
				j--;		
				if (bandera)
				{
					bandera=false;
					for(int k=j;k>i;k--){ 
						   if(arreglocopia[k-1].compareTo(arreglocopia[k])>=0){ 
						   T aux = arreglocopia[k];
						   arreglocopia[k] = arreglocopia[k-1];
						    arreglocopia[k-1] = aux; 
						    bandera=true;   
						 } 
						} 
				}
				i++;
		}
	return arreglocopia; 
	} 
}